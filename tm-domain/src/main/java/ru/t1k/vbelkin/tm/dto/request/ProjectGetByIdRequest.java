package ru.t1k.vbelkin.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public class ProjectGetByIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

}
