package ru.t1k.vbelkin.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.model.Task;

public class TaskChangeStatusByIndexResponse extends AbstractTaskResponse {

    public TaskChangeStatusByIndexResponse(@Nullable final Task task) {
        super(task);
    }

}
