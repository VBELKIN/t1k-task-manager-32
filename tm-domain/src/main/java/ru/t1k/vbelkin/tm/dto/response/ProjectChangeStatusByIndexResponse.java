package ru.t1k.vbelkin.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.model.Project;

public class ProjectChangeStatusByIndexResponse extends AbstractProjectResponse {

    public ProjectChangeStatusByIndexResponse(@Nullable final Project project) {
        super(project);
    }

}
