package ru.t1k.vbelkin.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.model.Task;

public class TaskRemoveByIdResponse extends AbstractTaskResponse {

    public TaskRemoveByIdResponse(@Nullable final Task task) {
        super(task);
    }

}
