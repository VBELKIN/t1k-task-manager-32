package ru.t1k.vbelkin.tm.exception.role;

public class ExistsEmailException extends AbstractUserException {

    public ExistsEmailException() {
        super("Error! Email already exists...");
    }

}
