package ru.t1k.vbelkin.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public class ProjectUpdateByIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    @Nullable
    private String name;

    @Nullable
    private String description;


}
