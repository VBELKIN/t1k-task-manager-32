package ru.t1k.vbelkin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1k.vbelkin.tm.api.endpoint.ISystemEndpoint;
import ru.t1k.vbelkin.tm.api.service.IPropertyService;
import ru.t1k.vbelkin.tm.api.service.IServiceLocator;
import ru.t1k.vbelkin.tm.dto.request.ServerAboutRequest;
import ru.t1k.vbelkin.tm.dto.request.ServerVersionRequest;
import ru.t1k.vbelkin.tm.dto.response.ServerAboutResponse;
import ru.t1k.vbelkin.tm.dto.response.ServerVersionResponse;

public class SystemEndPoint implements ISystemEndpoint {

    @NotNull

    private final IServiceLocator serviceLocator;

    public SystemEndPoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @NotNull
    public ServerVersionResponse getVersion(@NotNull ServerVersionRequest request) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

}
