package ru.t1k.vbelkin.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1k.vbelkin.tm.api.endpoint.*;
import ru.t1k.vbelkin.tm.api.repository.IProjectRepository;
import ru.t1k.vbelkin.tm.api.repository.ITaskRepository;
import ru.t1k.vbelkin.tm.api.repository.IUserRepository;
import ru.t1k.vbelkin.tm.api.service.*;
import ru.t1k.vbelkin.tm.dto.request.*;
import ru.t1k.vbelkin.tm.endpoint.*;
import ru.t1k.vbelkin.tm.enumerated.Role;
import ru.t1k.vbelkin.tm.enumerated.Status;
import ru.t1k.vbelkin.tm.model.User;
import ru.t1k.vbelkin.tm.repository.ProjectRepository;
import ru.t1k.vbelkin.tm.repository.TaskRepository;
import ru.t1k.vbelkin.tm.repository.UserRepository;
import ru.t1k.vbelkin.tm.service.*;
import ru.t1k.vbelkin.tm.util.SystemUtil;

import java.io.File;
import java.io.FileWriter;

@Getter
public final class Bootstrap implements IServiceLocator {

    private static final String PACKAGE_COMMANDS = "ru.t1k.vbelkin.tm.command";

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository(propertyService);

    @NotNull
    private final IUserService userService = new UserService(userRepository, projectService, taskService, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final Server server = new Server(this);

    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndPoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    {
        server.registry(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ServerVersionRequest.class, systemEndpoint::getVersion);

        server.registry(ProjectChangeStatusByIdRequest.class, projectEndpoint::changeProjectStatusById);
        server.registry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::changeProjectStatusByIndex);
        server.registry(ProjectClearRequest.class, projectEndpoint::clearProject);
        server.registry(ProjectCreateRequest.class, projectEndpoint::createProject);
        server.registry(ProjectListRequest.class, projectEndpoint::listProject);
        server.registry(ProjectRemoveByIdRequest.class, projectEndpoint::removeById);
        server.registry(ProjectRemoveByIndexRequest.class, projectEndpoint::removeByIndex);
        server.registry(ProjectUpdateByIdRequest.class, projectEndpoint::updateById);
        server.registry(ProjectUpdateByIndexRequest.class, projectEndpoint::updateByIndex);

        server.registry(UserChangePasswordRequest.class, userEndpoint::changeUserPassword);
        server.registry(UserLockRequest.class, userEndpoint::lockUser);
        server.registry(UserUnlockRequest.class, userEndpoint::unlockUser);
        server.registry(UserRemoveRequest.class, userEndpoint::removeUser);
        server.registry(UserUpdateProfileRequest.class, userEndpoint::updateUserProfile);
        server.registry(UserRegistryRequest.class, userEndpoint::registryUser);

        server.registry(DataBackupLoadRequest.class, domainEndpoint::loadDataBackup);
        server.registry(DataBackupSaveRequest.class, domainEndpoint::saveDataBackup);
        server.registry(DataBase64LoadRequest.class, domainEndpoint::loadDataBase64);
        server.registry(DataBase64SaveRequest.class, domainEndpoint::saveDataBase64);
        server.registry(DataBinaryLoadRequest.class, domainEndpoint::loadDataBinary);
        server.registry(DataBinarySaveRequest.class, domainEndpoint::saveDataBinary);
        server.registry(DataJsonLoadFasterXmlRequest.class, domainEndpoint::loadDataJsonFasterXml);
        server.registry(DataJsonLoadJaxBRequest.class, domainEndpoint::loadDataJsonJaxB);
        server.registry(DataJsonSaveJaxBRequest.class, domainEndpoint::saveDataJsonJaxB);
        server.registry(DataXmlLoadFasterXmlRequest.class, domainEndpoint::loadDataXmlFasterXml);
        //server.registry(DataXmlLoadJaxBRequest.class, domainEndpoint::loadDataXmlJaxB);
        server.registry(DataXmlSaveFasterXmlRequest.class, domainEndpoint::saveDataXmlFasterXml);
        //server.registry(DataXmlSaveJaxBRequest.class, domainEndpoint::saveDataXmlJaxB);
        server.registry(DataYamlLoadFasterXmlRequest.class, domainEndpoint::loadDataYamlFasterXml);
        server.registry(DataYamlSaveFasterXmlRequest.class, domainEndpoint::saveDataYamlFasterXml);
        server.registry(DataJsonSaveFasterXmlRequest.class, domainEndpoint::saveDataJsonFasterXml);

        server.registry(TaskBindToProjectRequest.class, taskEndpoint::bindTaskToProject);
        server.registry(TaskChangeStatusByIdRequest.class, taskEndpoint::changeTaskStatusById);
        server.registry(TaskChangeStatusByIndexRequest.class, taskEndpoint::changeTaskStatusByIndex);
        server.registry(TaskClearRequest.class, taskEndpoint::clearTask);
        server.registry(TaskCreateRequest.class, taskEndpoint::createTask);
        server.registry(TaskListByProjectIdRequest.class, taskEndpoint::listTaskByProjectId);
        server.registry(TaskListRequest.class, taskEndpoint::listTask);
        server.registry(TaskRemoveByIdRequest.class, taskEndpoint::removeById);
        server.registry(TaskRemoveByIndexRequest.class, taskEndpoint::removeByIndex);
        server.registry(TaskUnbindFromProjectRequest.class, taskEndpoint::unbindTaskFromProject);
        server.registry(TaskUpdateByIdRequest.class, taskEndpoint::updateById);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::updateByIndex);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task_manager.pid";
        final long pid = SystemUtil.getPID();
        File file = new File(fileName);
        FileWriter myWriter = new FileWriter(file);
        myWriter.write(Long.toString(pid));
        myWriter.close();
        file.deleteOnExit();
    }

    private void initDemoData() {
        final User user1 = userService.create("test", "test", Role.USUAL);
        final User admin = userService.create("admin", "admin", Role.ADMIN);

        projectService.create(admin.getId(), "First", "First Project").setStatus(Status.NOT_STARTED);
        projectService.create(admin.getId(), "Second", "Second Project").setStatus(Status.IN_PROGRESS);
        projectService.create(user1.getId(), "Third", "Third Project").setStatus(Status.NOT_STARTED);
        projectService.create(user1.getId(), "Force", "Force be with you").setStatus(Status.COMPLETED);

        taskService.create(admin.getId(), "Third", "Third Task");
        taskService.create(admin.getId(), "Second", "Second Task");
        taskService.create(user1.getId(), "First", "First Task");
    }

    private void prepareShutdown() {
        loggerService.info("**TASK-MANAGER SERVER IS SHUTTING DOWN**");
        backup.stop();
        server.stop();
    }

    public void run() {
        initPID();
        initDemoData();
        loggerService.info("**TASK-MANAGER SERVER STARTED**");
        backup.start();
        server.start();
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

}
