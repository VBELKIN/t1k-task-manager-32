package ru.t1k.vbelkin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.dto.request.UserLogoutRequest;
import ru.t1k.vbelkin.tm.enumerated.Role;

public class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "logout";

    @NotNull
    private static final String DESCRIPTION = "logout current user";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        @NotNull final UserLogoutRequest request = new UserLogoutRequest();
        getUserEndpointClient().logout(request);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
