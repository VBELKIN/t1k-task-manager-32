package ru.t1k.vbelkin.tm.api.endpoint;

import ru.t1k.vbelkin.tm.dto.request.AbstractRequest;
import ru.t1k.vbelkin.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request);

}

