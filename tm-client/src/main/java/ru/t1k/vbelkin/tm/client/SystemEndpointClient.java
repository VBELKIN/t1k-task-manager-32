package ru.t1k.vbelkin.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1k.vbelkin.tm.api.endpoint.ISystemEndpoint;
import ru.t1k.vbelkin.tm.dto.request.ServerAboutRequest;
import ru.t1k.vbelkin.tm.dto.request.ServerVersionRequest;
import ru.t1k.vbelkin.tm.dto.response.ServerAboutResponse;
import ru.t1k.vbelkin.tm.dto.response.ServerVersionResponse;

@NoArgsConstructor
public final class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpoint {

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        @NotNull final ServerAboutResponse serverAboutResponse = client.getAbout(new ServerAboutRequest());
        System.out.println(serverAboutResponse.getEmail());
        System.out.println(serverAboutResponse.getName());
        @NotNull final ServerVersionResponse serverVersionResponse = client.getVersion(new ServerVersionRequest());
        System.out.println(serverVersionResponse.getVersion());
        client.disconnect();
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        return call(request, ServerAboutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        return call(request, ServerVersionResponse.class);
    }

}

