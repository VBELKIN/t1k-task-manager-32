package ru.t1k.vbelkin.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.dto.request.DataYamlSaveFasterXmlRequest;

public class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private static final String NAME = "data-save-yaml";

    @NotNull
    private static final String DESCRIPTION = "Save data in yaml file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE YAML]");
        @NotNull DataYamlSaveFasterXmlRequest request = new DataYamlSaveFasterXmlRequest();
        getDomainEndpointClient().saveDataYamlFasterXml(request);
    }

}
