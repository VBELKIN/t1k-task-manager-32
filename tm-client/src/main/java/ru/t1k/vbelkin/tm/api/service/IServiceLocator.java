package ru.t1k.vbelkin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1k.vbelkin.tm.api.client.IEndPointClient;
import ru.t1k.vbelkin.tm.client.*;

public interface IServiceLocator {

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IEndPointClient getConnectionEndpointClient();

    @NotNull
    AuthEndpointClient getAuthEndpointClient();

    @NotNull
    DomainEndpointClient getDomainEndpointClient();

    @NotNull
    ProjectEndpointClient getProjectEndpointClient();

    @NotNull
    TaskEndpointClient getTaskEndpointClient();

    @NotNull
    SystemEndpointClient getSystemEndpointClient();

    @NotNull
    UserEndpointClient getUserEndpointClient();

}
